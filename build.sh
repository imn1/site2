#!/bin/bash

# Prune dist directory.
rm -rf dist/*

# Build templates.
(cd builder; cargo run)

# Copy assets over.
cp src/*.css dist
cp -r src/img dist
cp -r src/other dist
rm dist/other/cv.tex

# Convert images.
## dtops
cd dist/img/dtops

for file in *
do
    convert -thumbnail 150x100 -format png $file ${file%.*}_tmb.png
done

## designs
cd ../design
shopt -s globstar

for file in **/*
do
    if [[ -f $file ]]
    then
        pwd
        echo $file
        echo ${file%.*}_tmb.png
        convert -thumbnail 150x100 -format png $file ${file%.*}_tmb.png
    fi
done

cd ../../../

if [[ "$1" != "nopdf" ]]; then
    # Create PDF version of posts.
    for file in $(find src/posts -name "*.md"); do

        DOC_PATH=dist/posts/$(basename ${file%.*}).tex

        pandoc -s --template latex.template -f markdown -t latex -o $DOC_PATH $file
        # patch image files: /img/ -> ../../src/img/
        sed -i "s/\/img\//..\/..\/src\/img\//g" $DOC_PATH
        tectonic $DOC_PATH

        # clean up .tex files - unneded after conversion
        rm -f $DOC_PATH
    done
fi

# CV
tectonic -o dist/other src/other/cv.tex 
