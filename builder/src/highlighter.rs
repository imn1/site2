use std::{
    fs::{read_to_string, write},
    io::{self, Cursor, Write},
    path::Path,
};

use htmlentity::entity::decode;
use regex::Regex;
use syntect::highlighting::ThemeSet;
use syntect::html::highlighted_html_for_string;
use syntect::parsing::SyntaxSet;

use crate::ROOT;

/// Finds programming language name (if any) in
/// the given <pre> content based on inner <code>
/// class name.
fn get_language(pre: &str) -> Option<String> {
    let re = Regex::new(r#"class="language-(.+?)""#).unwrap();

    let captures = re.captures(pre);

    if captures.is_none() {
        return None;
    }

    captures.unwrap().get(1).map(|c| c.as_str().to_string())
}

// Highlights the given code according to the given
// language (if found in highlighter). If not found
// returns the original str.
fn highlight(code: &str, language: &str) -> String {
    let ss = SyntaxSet::load_defaults_newlines();
    let mut theme_cursor = Cursor::new(include_bytes!("../emby.tmTheme"));
    let theme = ThemeSet::load_from_reader(&mut theme_cursor).unwrap();

    if let Some(syntax) = ss.find_syntax_by_token(language) {
        return highlighted_html_for_string(code, &ss, syntax, &theme)
            .expect(format!("Couldn't highlight code: {}", code).as_str());
    }

    // By default we return what we got.
    code.to_string()
}

fn replace_at(strb: &mut Vec<u8>, start: usize, end: usize, substitution: String) {
    let _removed: Vec<_> = strb.splice(start..end, substitution.bytes()).collect();
    // println!("Removed from string: {:?}", &removed);
}

/// Seeks for <pre><code>...</code></pre> pairs and
/// tries to highlight them based on CSS class of
/// <code> tag.
pub fn highlight_code() {
    for entry in globwalk::glob(format!(
        "{}/*.html",
        Path::new(ROOT)
            .join("dist")
            .join("posts")
            .into_os_string()
            .into_string()
            .expect("Cannot walk thru post files.")
    ))
    .expect("Cannot walk thru post files.")
    {
        match entry {
            Ok(file) => {
                // println!("{}", "-".repeat(100));
                // println!("{}", file.path().display());
                // println!("{}", "-".repeat(100));
                print!("Highlighting {} ... ", file.file_name().to_string_lossy());

                // Needed so above line is printed out immediately.
                io::stdout().flush().unwrap();

                // 1. read file into string and bytes.
                let mut content = read_to_string(file.path()).expect(
                    format!("Cannot read content of a file: {}", file.path().display()).as_str(),
                );
                // Serves as a starting point (byte) which is used for slicing
                // current (file) content from.
                let mut last_b: usize = 0;

                // 2. construct regex.
                let re = Regex::new(r"(?s)<pre>(<code[^>]*>(.+?)</code>)</pre>").unwrap();

                loop {
                    // 3. get first (left-most) captures.
                    // captures are:
                    // 0 - <pre> element - whole regex
                    // 1 - <code> element
                    // 2 - <code> content - the code

                    // 3. create slices (str + bytes) based on last byte we worked with (last_b)
                    let content_slice_b = content.as_bytes()[last_b..].to_vec();
                    let content_slice = String::from_utf8(content_slice_b.clone())
                        .expect("Cannot construct string from remaining file content.");

                    // 4. find code we will highlight.
                    let captures = re.captures(&content_slice);
                    match captures {
                        Some(find) => {
                            let pre = find.get(0).unwrap();
                            // println!("Start: {}, End: {}", pre.start(), pre.end());
                            // println!("Last b: {}", &last_b);
                            // println!("Pre: {:?}", pre.as_str());

                            let code = find.get(1).unwrap();
                            let code_content = find.get(2).unwrap().as_str();

                            // 5. decode code HTML entities.
                            let decoded: Vec<char> = decode(&code_content);
                            let code_decoded = decoded.iter().collect::<String>();

                            // 6. determine language and highlighting (if possible).
                            let language = get_language(code.as_str());

                            // 7. replace original code (content) with highlighted one + determine
                            // last byte which we worked with - end byte of (non-)highlighted
                            // code.
                            match language {
                                Some(syntax) => {
                                    let highlighted_code = highlight(&code_decoded, &syntax);
                                    let next_last_b = pre.start() + highlighted_code.len();
                                    let mut content_b = content.into_bytes();
                                    replace_at(
                                        &mut content_b,
                                        pre.start() + last_b,
                                        pre.end() + last_b,
                                        highlighted_code.clone(),
                                    );
                                    last_b = next_last_b;
                                    // Recreate content from spliced bytes.
                                    content = String::from_utf8(content_b).expect(
                                        "Cannot construct string from remaining file content.",
                                    );
                                    // println!("Replaced with: {}", highlighted_code);
                                    // println!("Content so far: {}", &content);
                                    // println!("{}");
                                }
                                // We haven't highlighted any code but we need to save
                                // last byte we worked with.
                                None => last_b = pre.end(),
                            }
                        }
                        // In case there is no capture we are done with this file.
                        None => break,
                    }
                }

                // 9. write final content under original file path.
                write(file.path(), content.as_bytes()).expect(
                    format!(
                        "Cannot write highlighted code to: {}",
                        file.path().display()
                    )
                    .as_str(),
                );

                println!("highlighted.");
            }
            Err(err) => {
                panic!("Cannot work with post entry: {}", err);
            }
        }
    }
}
