use crate::ROOT;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;
use tera::{to_value, Result as TeraResult, Value};

use heck::ToTitleCase;

/// Reads all PDF and EPUB files from books directory
/// and creates a sorted list pairs of mimetype and
/// url to the file.
pub fn books(_args: &HashMap<String, Value>) -> TeraResult<Value> {
    let mut books = vec![];

    for entry in globwalk::GlobWalkerBuilder::from_patterns("../books", &["*.{pdf,epub}"])
        .sort_by(|a, b| Ord::cmp(a.file_name(), b.file_name()))
        .build()
        .expect("Couldn't read directory with books")
    {
        match entry {
            Ok(file) => books.push((
                file.path()
                    .extension()
                    .unwrap_or_else(|| {
                        panic!(
                            "File {} has no extension.",
                            file.path().file_name().unwrap().to_string_lossy()
                        )
                    })
                    .to_string_lossy()
                    .into_owned(),
                file.file_name().to_string_lossy().into_owned(),
            )),
            Err(err) => panic!("Couldn't work with a book entry: {}", err),
        }
    }

    Ok(to_value(books).expect("Couldn't serialize books."))
}

/// Returns Vec of title-path pairs.
pub fn posts(_args: &HashMap<String, Value>) -> TeraResult<Value> {
    let mut posts = vec![];

    // All markdown files from ../posts directory.
    for entry in globwalk::GlobWalkerBuilder::from_patterns(
        Path::new(ROOT).join("src").join("posts"),
        &["*.md"],
    )
    // Sorts by file name.
    // Each post has a XXX prefix which is a number - fixed size.
    // If not system default to 0 which results such post(s) will
    // be always as first.
    .sort_by(|a, b| {
        let file_name_a = a.file_name().to_string_lossy().to_string();
        let (number_a, _) = file_name_a.split_once('_').unwrap();

        let file_name_b = b.file_name().to_string_lossy().to_string();
        let (number_b, _) = file_name_b.split_once('_').unwrap();

        let number_a: u32 = number_a.trim().parse().unwrap_or(0);
        let number_b: u32 = number_b.trim().parse().unwrap_or(0);

        number_a.cmp(&number_b)
    })
    .build()
    .expect("Couldn't read directory with posts.")
    .collect::<Vec<_>>()
    .into_iter()
    .rev()
    {
        match entry {
            Ok(file) => {
                let file_handle = File::open(file.path()).unwrap_or_else(|_| {
                    panic!("Couldn't open post file: {}", file.path().display())
                });
                let mut buffer = BufReader::new(file_handle);
                let mut title = String::new();
                buffer.read_line(&mut title).unwrap_or_else(|_| {
                    panic!("Cannot read from markdownfile: {}", file.path().display())
                });
                let title = title.strip_prefix('#').unwrap().trim().to_string();
                let path = file
                    .path()
                    .with_extension("html")
                    .file_name()
                    .unwrap()
                    .to_string_lossy()
                    .to_string();
                posts.push((title, path))
            }
            Err(err) => panic!("Couldn't read path: {}", err),
        }
    }

    Ok(to_value(posts).expect("Couldn't serialize posts."))
}

/// Returns Vec of path-thumbnail path-title-year items.
pub fn designs(_args: &HashMap<String, Value>) -> TeraResult<Value> {
    let mut designs = vec![];

    for entry in globwalk::GlobWalkerBuilder::from_patterns(
        Path::new(ROOT).join("src").join("img").join("design"),
        &["*.{png,jpg}"],
    )
    .sort_by(|a, b| {
        // Parse year out of the first filename.
        let year_a: u32 = a
            .file_name()
            .to_string_lossy()
            .to_string()
            .split('.')
            .next()
            .unwrap()
            .split('_')
            .last()
            .unwrap_or_else(|| panic!("Not every design has a year in it's filename."))
            .parse()
            .unwrap_or_else(|err| panic!("Cannot parse a year from a filename {}.", err));

        // Parse year out of the second filename.
        let year_b: u32 = b
            .file_name()
            .to_string_lossy()
            .to_string()
            .split('.')
            .next()
            .unwrap()
            .split('_')
            .last()
            .unwrap_or_else(|| panic!("Not every design has a year in it's filename."))
            .parse()
            .unwrap_or_else(|err| panic!("Cannot parse a year from a filename {}.", err));

        Ord::cmp(&year_a, &year_b)
    })
    .build()
    .expect("Couldn't read directory with designs")
    {
        match entry {
            Ok(file) => {
                let filename = file.file_name().to_string_lossy().to_string();
                let path = file
                    .path()
                    .strip_prefix(Path::new(ROOT).join("src"))
                    .unwrap()
                    .to_owned();

                let mut tmb_filename = file.clone().into_path();
                tmb_filename.set_file_name(format!(
                    "{}_tmb.png",
                    file.clone()
                        .into_path()
                        .file_stem()
                        .unwrap()
                        .to_string_lossy()
                ));
                let tmb_filename = tmb_filename
                    .strip_prefix(Path::new(ROOT).join("src"))
                    .unwrap()
                    .to_path_buf()
                    .to_string_lossy()
                    .to_string();

                let mut filename_parts: Vec<&str> =
                    filename.split('.').next().unwrap().split('_').collect();

                let year: u32 = filename_parts.pop().unwrap().parse().unwrap();
                let title = filename_parts.join(" ").to_string().to_title_case();

                designs.push((path, tmb_filename, title, year))
            }
            Err(err) => panic!("Couldn't work with a design entry: {}", err),
        }
    }

    Ok(to_value(designs).expect("Couldn't serialize designs."))
}
