use std::{
    fs::{create_dir_all, read_to_string, File},
    io::Write,
    path::{Path, PathBuf},
};

use pulldown_cmark::{html, Parser};
use regex::{self, Regex};
use tera::{Context, Tera};

use crate::highlighter::highlight_code;

mod highlighter;
mod tera_functions;

const ROOT: &str = "/home/n1/workspace/site";

/// Creates necessary directories from the given path.
fn touch(path: &Path) {
    // Skip first item (file name)
    for anc in path.ancestors().skip(1) {
        if "" != anc.to_str().unwrap() {
            create_dir_all(Path::new(ROOT).join("dist").join(anc)).unwrap();
        }
    }
}

/// Builds all templates (recursivelly) from src/ directory.
fn build_templates() {
    // Init Tera with templates root.
    let mut tera = match Tera::new(format!("{}{}", ROOT, "/src/**/*.html").as_str()) {
        Ok(t) => t,
        Err(e) => {
            println!("Parsing error(s): {}", e);
            ::std::process::exit(1);
        }
    };

    tera.register_function("books", tera_functions::books);
    tera.register_function("posts", tera_functions::posts);
    tera.register_function("designs", tera_functions::designs);

    // Compile templates.
    for tpl in tera.get_template_names().filter(|i| {
        !Path::new(i)
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
            .starts_with('_')
    }) {
        print!("Compiling {} ", tpl);

        let path = Path::new(ROOT).join("dist").join(tpl);
        touch(&path);
        let mut buffer = File::create(path).unwrap();

        match tera.render_to(tpl, &Context::new(), &mut buffer) {
            Ok(_t) => println!("... compiled."),
            Err(err) => {
                panic!("Parsing error(s): {}", err);
            }
        }
    }
}

/// Build all markdown files from src/posts and stores
/// them under dist/posts location.
fn build_posts() {
    let skeleton = read_to_string(
        Path::new(ROOT)
            .join("src")
            .join("posts")
            .join("_skeleton.html"),
    )
    .expect("Cannot read post skeleton file.");

    for entry in globwalk::glob(format!(
        "{}/*.md",
        Path::new(ROOT).join("src").join("posts").to_string_lossy()
    ))
    .expect("Cannot walk thru markdown files.")
    {
        let content: String;
        let output_path: PathBuf;
        let mut html = String::new();

        // Load markdown file content.
        match &entry {
            Ok(file) => {
                content = read_to_string(file.path()).unwrap_or_else(|_| {
                    panic!("Cannot read content of a file: {}", file.path().display())
                });

                output_path = Path::new(ROOT)
                    .join("src")
                    .join("posts")
                    .join(file.file_name())
                    .with_extension("html");
            }
            Err(err) => {
                panic!("Cannot work with a markdown entry: {}", err);
            }
        }

        // Convert markdown -> html.
        touch(&output_path);
        html::push_html(&mut html, Parser::new(&content));

        // Direct all links that lead off the site to a new tab.
        html = links_to_new_tab(&html);
        html = images_to_new_tab(&html);

        // Wrap converted post HTML with post template skeleton.
        let mut baked_html = skeleton.replace("__POST_PLACEHOLDER__", &html);
        baked_html = baked_html.replace(
            "__PDF_URL__",
            format!(
                "{}.pdf",
                &entry.unwrap().path().file_stem().unwrap().to_str().unwrap()
            )
            .as_str(),
        );
        File::create(&output_path)
            .unwrap_or_else(|_| panic!("Cannot create file under path: {}", &output_path.display()))
            .write_all(baked_html.as_bytes())
            .unwrap_or_else(|_| {
                panic!(
                    "Cannot write baked post HTML to: {}",
                    &output_path.display()
                )
            });

        println!("Converted to HTML: {}", output_path.display());
    }
}

/// Converts all https links to be opened to a new tab.
/// This means all links to local site should be relative.
fn links_to_new_tab(html: &str) -> String {
    let regex = Regex::new(r#"(?m)<a href="https(.*?)"(.*?)>"#).unwrap();
    let substitution = "<a href=\"https$1\"$2 target=\"_blank\">";

    // result will be a String with the substituted value
    regex.replace_all(html, substitution).into_owned()
}

/// Converts all image links to be opened to a new tab.
fn images_to_new_tab(html: &str) -> String {
    let regex = Regex::new(r#"(?m)<a href="(.*?)\.(png|jpg)"(.*?)>"#).unwrap();
    let substitution = "<a href=\"$1.$2\"$3 target=\"_blank\">";

    // result will be a String with the substituted value
    regex.replace_all(html, substitution).into_owned()
}

fn main() {
    build_posts();
    build_templates();
    highlight_code();
}
