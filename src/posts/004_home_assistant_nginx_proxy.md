# Home Assistant Nginx proxy server

I run [Home Assistant](https://www.home-assistant.io)
instance at home and I wanna be able to control it by
NFC tags. That is quite easy task as long as HA has
API and
[NFC Tasks](https://play.google.com/store/apps/details?id=com.wakdev.nfctasks)
app exists. The real problem is that the app doesn't
support custom headers in HTTP requests and since
HA REST API uses header `Authorization` to authorize
requests whole setup won't work.

That's when Nginx proxy comes to help. Setting forward
proxy that just hands over all incoming requests and is able
to modify them (like to add the desired header) is
a perfect soultion and quite simple one. Nginx config
is all what's needed:

```
server {
    listen 80;

    resolver 192.168.88.20;

    location / {

        proxy_set_header Authorization "Bearer MY_SECRET_TOKEN";
        proxy_pass http://o2.home:8123$request_uri;
    }
}
```

- `resolver` points to my local DNS server - required
- `proxy_set_header` sets `Authorization` header for each
  request where the secret token is a
  [long lived token](https://developers.home-assistant.io/docs/auth_api/#long-lived-access-token)
- `proxy_pass` redirects all requests to my local HA
  instance (the server domain name is resolved thru
  `resolver`)

Now all requests made to the proxy will be authorized
in HA.
