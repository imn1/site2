# How I solved the dilemma of personal notes

Personal notes are a big topic across the whole universe
and tackles pretty much everyone who sits behind keyboard
and wants to use more sophisticated way of persistent notes
than pen and paper. No hate to pen and paper which I use
a lot to keep my handwriting in condition and because for
short-term notes it's the best option. But you cannot sync
them across devices nor dynamically update them like insert
a new list item between 1st and 2nd one. Or copy the list
content to an email message.

## Cloud service providers
Pretty much every other online service is about notes. Like
there is so much notes services that it's hard to choose one.
Lets enumerate the most known:

- [Evernote](https://evernote.com)
- [Notion](https://www.notion.so)
- [Google Keep](https://www.google.com/keep/)
- [OneNote](https://www.onenote.com)
- [Simplenote](https://simplenote.com/)

Each service has it pros and cons where pros can be number
of features and cons is basically the opposite - lack of
features. The thing is the more features a note taking
service provides the more tied to that an user becomes. I
mean the notes are becoming importable to another service
of even exportable in editable format - not just PDF. That's
the main point that made me think when I was using Notion
as my main note taking service across all my devices.

## Cloud service applications
The second most important point is how the service supports
its client devices. Mainly mobile devices.

Since corporations stopped caring how their apps perform
and started massively utilizing such mammoth as
[Electron](https://old.reddit.com/r/linux/comments/ybebq1/what_makes_people_hate_electron/)
which is slow, memory hungry with huge binaries with zero OS
integration (looking at you MS Teams, Slack, Discord, Spotify
Notion, etc.), it's very hard to even care. There are note
taking apps out there that are 400MB, very slow and take
500MB of RAM. Therefore my personal take was that I gave up
on desktop app. No need to keep open another browser (Electron
instance) with Xbox drivers running in background to create
my shopping list. However I had to sacrifice my phone and tablet
for Notion slow app which takes 6 seconds to even open up on
8 core with 4GB RAM
[Lenovo M10 Plus](https://www.gsmarena.com/lenovo_m10_plus-10360.php).
Phone was quite faster but still incredibly slow particularly in
opening notes - a few seconds.

Looks like creating a native app that has 3 screens (list of
notes, editor, preview) is too much work for companies that
charge their service or are backed up by huge corporations.

So:

- desktop: use browser with pinned tab
- tablet: slow app
- phone: slow app

## Desktop applications
Of course there is an old-school approach - a desktop app. The
thing is that today's programmers forgot how to create a native
desktop app in languages like C/C++ or any other language that
compiles thus can be run without any crutch (interpreter, web browser, ...),
is fast, doesn't eat memory - simply said it's a lightweight application
for lightweight task. And no, JavaScript is not a language suitable for
desktop development.

Although there are some exceptions in that mess:

- [QOwnNotes](https://www.qownnotes.org)
- [cherrytree](https://www.giuspen.net/cherrytree/)
- [Gnote](https://gitlab.gnome.org/GNOME/gnote)

These apps look good but they only serve for local notes and in
case you use more advanced features like LaTeX (or its variants)
or calendar you do need some sort of rendering. You cannot
fully use them on mobile device without companion app.

## Exports/migrations
This is very service specific. Some give the ability to
export each note in PDF and mainly at least in Markdown. However
by exporting one might lose almost all the features of a component
that was part of a note - drawings, embeds (like Trello), boards etc.

I kept my notes intentionally very simple with no fancy components
so I could export and reuse them once Notion cancels free plan
or announces they are closing down. Been there so I kept that on
my mind.

Exports to PDF were nifty so I could share my notes with others
while they looked good since Notion PDF template was nice and gave
you the possibility to scale text and images.

## Services conclusion
Taking into account:

- lousy apps
- free but in exchange for "can cancel anytime"
- limited exports
- no physical (human readable) files to own
- have to give up your e-mail
- very primitive thing must have very primitive solution

I decided to close my account at Notion and come up with my easy
setup where I can take it to another level with simple scripts
which I couldn't till now.

## My solution
I wanted:

- simple notes
- to be easily edited (I'm a NeoVim user)
- synchronized
- no pseudo or binary format
- taking advantage of ASCII art (no need to draw a simple chart)

So I came up following ultimate setup that is very simple
and has a very few points-of-failure:

- simple markdown files organized into directories
- synchronized onto my home Nextcloud instance with:
    - on desktop with [Nextcloud Client](https://nextcloud.com/install)
    - on phone/tablet with [Nextcloud Notes](https://f-droid.org/en/packages/it.niedermann.owncloud.notes/)
- edited with NeoVim (with spellcheck support)
- with shortcut for my main TODO.md note in my WM (bspwm)

This solution:

- does not require any other app (an editor)
- I can utilize my editor (NeoVim) habits
- is super fast
- consumes kilobytes in disk size instead of hundred megabytes
- consumes ~20MB instead of 0.5GB of memory (all my NeoVim plugins)
- I do own my notes
- can be easily backed up
- I can share them within a few seconds (no need for export)
- can be exported in any format (rtf, pdf, tex)
- I can choose any export template I want (a fancy one for corporate notes)
- no need to worry about service cancellation
- can be read programmatically
- easy backup

As a desktop integration I come up with one alias and one simple
script. The alias is `vt` and meant "vim todo". The alias is fairly
simple:

```bash
$ cd ~/nextcloud/Notes/; nvim -c ZenMode TODO.md
```

1. move to my notes directory
2. choose my TODO note
3. open the note in NeoVim
4. activate ZenMode

I do have a script with the same name `vt` which can be run
from [rofi](https://github.com/davatorium/rofi):

```bash
 #!/bin/bash

 cd ~/nextcloud/Notes/
 bspc rule -a Alacritty -o state=floating center=true rectangle=400x600+0+0
 /home/n1/.cargo/bin/alacritty -e /home/n1/bin/nvim -c ZenMode TODO.md
```

Does exactly the same like my alias but opens up the TODO note
in a new small-sized terminal in the center of my desktop. So
I can easily access my main note + other notes via my editor
because all notes lie in the same root (sub)directory.

![todo notes](/img/posts/008/todo.png)
