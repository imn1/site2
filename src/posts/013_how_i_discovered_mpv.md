# How I discovered MPV

A while ago I got a new laptop from Tuxedo Computers.
It was ~7 years since my last laptop purchase, so I was
quite happy. I also decided to advance in my setup and I
decided to give the Wayland thing a shot. If it won't
work according to my standards, I will roll back to the
tearing X11. I knew this change comes with a toll of new apps.
For example, I switched from
[bspwm](https://github.com/baskerville/bspwm) to
[Hyprland](https://hyprland.org/). Since I don't use many GUI
apps, I didn't have to change every app. But one that let me down
was my very favorite TUI music player [moc](https://moc.daper.net/).
Why moc? No library, no ID3 tags, just folders, files and
the player. Exactly what I needed. Well, in the time of the new
setup install process, Debian had no moc package for unstable
release. Newer and older packages didn't work. Never mind,
maybe it's time to try a new app - let's see what's around.

## ncmpcpp

Very popular, written in C++ and well maintained. But it's
all about music library and tags. That won't work for me since
90% of my library are sets/mixes downloaded from video portals.
So no artist names, no song names, no album names, no tags.
Never mind, let's see what's next.

[ncmcpp homepage](https://github.com/ncmpcpp/ncmpcpp)

## cmus

Pretty much the same as ncmpcpp but it's written in C and
the maintenance is sparse. Never mind, let's see what's next.

[cmus homepage](https://cmus.github.io/)

## musikcube

C++, well maintained and.... music library-based. Well, it seems
like the basic directory-based approach won't work here either.

[musikcube homepage](https://github.com/clangen/musikcube)

## Frustration

I wish my favorite Midnight Commander could play music.... wait!
It can't but it can spawn a player ... a CLI player that can
play it for me. Once I exit the player, I end up in the directory
structure again. Damn, that's exactly what I'm looking for.

# MPV

I used (on my old setup) SMPlayer for years. I already knew some
keyboard shortcuts and I had no issue with this software whatsoever.
But it cannot play stuff in CLI. Obviously, it's a GUI frontend.
Well, maybe it's time to try what's beneath that GUI.

I was very surprised when I opened a music track in MPV and I got...
well not a TUI but just some kind of progress bar and track info...
and working keyboard shortcuts! I've never seen that in any CLI
app for ~20 years. The only keyboard "shortcut" that worked for
running commands was Ctrl-C :)

Well, it seems like I did discover the beauty of MPV. I knew
that if I throw a YouTube URL on it, it will play it. If I pass
`--no-video` parameter I get audio only (battery saver). I can
also give not just a file but a whole directory so I get a queue.
Yes, it worked. Looks like I don't need SMPlayer or moc anymore.

[mpv homepage](https://mpv.io/)

# MPV tweaks

Fast-forward 2 week and I discovered
[this](https://github.com/Samillion/ModernZ) - a theme - because
the default one is just ugly. Also I installed
[this](https://github.com/po5/thumbfast) and got timeline thumbnails.
Now I was very happy man and I haven't dove into configuration
rabbit hole yet. Well maybe I don't need to tweak any stuff since
I believe in sane defaults.

