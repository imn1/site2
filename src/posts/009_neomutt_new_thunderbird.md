# NeoMutt: new Thunderbird

I've used Thunderbird ... a lot... actually every day since 
Google [ditched](https://en.wikipedia.org/wiki/Inbox_by_Gmail)
Inbox. It was my daily driver for emails with 4 mailboxes. I'm
a [gruvbox lover](/dtops.html) so I've installed
[gruvbox theme](https://addons.thunderbird.net/en-US/thunderbird/addon/gruvbox-dark-thunderbird/)
and modified
[userChrome.css](https://support.mozilla.org/en-US/questions/1160703)
and
[userContent.css](https://support.mozilla.org/en-US/questions/1320156)
files so my email client looked even more gruvboxy with
the same font I use throughout my whole system.

One day Mozilla came up with an UI overhaul called
[Supernova](https://www.thunderbird.net/en-US/thunderbird/115.0/whatsnew/).
This change turned Thunderbird into Outlook layout
in default layout. Anyway half of my userChrome setup
broke down. My font set-up was gone. Counting in the
memory consumption this was the last straw on camels
back. I had to migrate to something else.

## Choices

Let's enumerate what options I had:

- Evolution - tied to Gnome
- KMail - tied to KDE
- Geary - not bad but uses Outlook-like layout
- Blue Mail - proprietary
- Claws Mail - used in past but the UI is ugly even for me
- Mail Spring - Electron ...
- a few other Electron apps ...
- a few discontinued apps ...
- NeoMutt - CLI app - kinda rough

Long story short, except Thunderbird, there is no
good e-mail client for Linux if you use WM not DE
and you don't wanna go CLI. But wait... I like CLI
(nvim, mocp, mc, weechat, ....) so I went for NeoMutt.
Maybe it won't  be that difficult + I will free some MB
from my memory.

## NeoMutt setup

First I went thru a few (
i.e. [here](https://gideonwolfe.com/posts/workflow/neomutt/intro/)
) blog posts so I was able to imagine what actually NeoMutt  is.
Also NeoMutt's homepage offers pretty good
[guide](https://neomutt.org/guide/) as an entry point. I
recommend to read the whole guide. It's worth the time.

The blog post I linked above is good and goes thru the complex
setup with a GMail mailbox - which was my case. Except setting
a custom password for external application there was no
issue whatsoever. My NeoMutt instance could connect to
GMail and fetch all my mails - really fast - actually
much faster than Thunterbird.

It's worth to mention there is quite a difference in
setting up following directives

- `header_cache`
- `message_cachedir`

to file paths or to directory paths. In case of file
NeoMutt uses that file as one large shared database file.
In case of directory NeoMutt uses the directory as a root
directory for caching stuff in more separated files.
I went for option 2 because it performs faster.

NeoMutt has a ton of
[configuration directives](https://neomutt.org/guide/reference.html)
therefore I went step by step - sidebar -> index (list of emails) ->
pager (email message detail) -> .... I configured every
piece of NeoMutt step by step so I get the expected behavior
but wasn't overwhelmed by new stuff.

I also focused first on reading emails then on the NeoMutt
machinery of composing emails. That prevented me from losing
track what is what and gave me some time to get used to
keybindings.

The good advice is to keep config separated in a few individual
files and then `souce` them in the main `neomuttrc` file.

```
         File: neomuttrc

   1     source ./accounts
   2     source ./sidebar
   3     source ./index
   4     source ./macros
   5     source ./pager
   6     source ./ux
   7     source ./keys
   8     source ./theme
```

This helps to keep stuff organized by it's intent. Another
step not to get lost in all the NeoMutt's config directives.

[Here](https://gitlab.com/-/snippets/3615769) is my config
(current to this date). You can check it out and get inspired.
I removed most of unexpected (compared to Thunderbird) behavior,
set up Firefox as default HTML emails viewer (some HTML emails
contain HTML errors which the default viewer is not able to
render), and set up a macro for saving attachments to disk.

## First impressions
I've been using NeoMutt for like 4 weeks and I had no
hard times. Surprisingly. I'm used to CLI so no potential
hurdles here but I was surprised how intuitive the UI
is and how easy it's to get used to a few new keybindings.

After I tuned up the config the experience was pretty
great. I started noticing the speed of CLI (which is why I
use most of CLI apps) and different memory footprint - a
fraction of what Thunderbird was taking. Handling NeoMutt
is in fact much easier than I was thinking. 

## Screenshots

(please keep in mind that the look is my custom gruvbox theme)

[![neomutt screenshot](/img/posts/009/neomutt_screenshot_0.png)](/img/posts/009/neomutt_screenshot_0.png)

[![neomutt screenshot](/img/posts/009/neomutt_screenshot_1.png)](/img/posts/009/neomutt_screenshot_1.png)

[![neomutt screenshot](/img/posts/009/neomutt_screenshot_2.png)](/img/posts/009/neomutt_screenshot_2.png)
